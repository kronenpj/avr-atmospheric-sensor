EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATmega:ATmega328P-PU U1
U 1 1 5EEEABD5
P 1950 2650
F 0 "U1" H 1306 2696 50  0000 R CNN
F 1 "ATmega328P-PU" H 1306 2605 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm_Socket_LongPads" H 1950 2650 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 1950 2650 50  0001 C CNN
	1    1950 2650
	1    0    0    -1  
$EndComp
$Comp
L avr-atmospheric-sensor:BME280BreakoutI2C U2
U 1 1 5EEE9F87
P 4450 1550
F 0 "U2" H 4021 1596 50  0000 R CNN
F 1 "BME280BreakoutI2C" H 4021 1505 50  0000 R CNN
F 2 "avr-atmospheric-sensor:BME280-SparkFun-BreakOut-+Header" H 5950 1100 50  0001 C CNN
F 3 "https://ae-bst.resource.bosch.com/media/_tech/media/datasheets/BST-BME280-DS002.pdf" H 4450 1350 50  0001 C CNN
	1    4450 1550
	1    0    0    -1  
$EndComp
Text GLabel 5050 1500 2    50   Input ~ 0
SCL
Text GLabel 5050 1650 2    50   Input ~ 0
SDA
Text GLabel 2550 2850 2    50   Input ~ 0
SCL
Text GLabel 2550 2750 2    50   Input ~ 0
SDA
Text GLabel 1950 1150 1    50   Input ~ 0
VCC
Text GLabel 2050 1150 1    50   Input ~ 0
VCC
Text GLabel 1950 4150 3    50   Input ~ 0
GND
Text GLabel 2550 3150 2    50   Input ~ 0
RX
Text GLabel 2550 3250 2    50   Input ~ 0
TX
Text GLabel 2550 2950 2    50   Input ~ 0
~RESET
$Comp
L avr-atmospheric-sensor:USB-to-TTYSerial J1
U 1 1 5EEEACF0
P 4400 3550
F 0 "J1" H 4100 3950 50  0000 C CNN
F 1 "USB-to-TTYSerial" H 4400 3500 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 4550 3450 50  0001 C CNN
F 3 "" H 4250 3550 50  0001 C CNN
	1    4400 3550
	1    0    0    -1  
$EndComp
Text GLabel 4400 3050 1    50   Input ~ 0
VCC
Text GLabel 4400 4050 3    50   Input ~ 0
GND
Text GLabel 4350 2150 3    50   Input ~ 0
GND
Text GLabel 3900 3350 0    50   Input ~ 0
~RESET
Text GLabel 4900 3350 2    50   Input ~ 0
RX
Text GLabel 4900 3450 2    50   Input ~ 0
TX
$Comp
L avr-atmospheric-sensor:OLED_0.96_I2C U3
U 1 1 5EEEC9D4
P 6050 1000
F 0 "U3" H 5925 1015 50  0000 C CNN
F 1 "OLED_0.96_I2C" H 5925 924 50  0000 C CNN
F 2 "avr-atmospheric-sensor:OLED_0.91in_I2C+Header" H 6050 1000 50  0001 C CNN
F 3 "" H 6050 1000 50  0001 C CNN
	1    6050 1000
	1    0    0    -1  
$EndComp
Text GLabel 6250 1350 2    50   Input ~ 0
VCC
Text GLabel 6250 1250 2    50   Input ~ 0
GND
Text GLabel 5600 1350 0    50   Input ~ 0
SDA
Text GLabel 5600 1250 0    50   Input ~ 0
SCL
$Comp
L avr-atmospheric-sensor:In-Circuit-Program J2
U 1 1 5EEEE1B0
P 6050 3550
F 0 "J2" H 5750 3950 50  0000 C CNN
F 1 "In-Circuit-Program" H 6450 3950 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 6200 3450 50  0001 C CNN
F 3 "" H 5900 3550 50  0001 C CNN
	1    6050 3550
	1    0    0    -1  
$EndComp
Text GLabel 5550 3350 0    50   Input ~ 0
~RESET
Text GLabel 6050 3050 1    50   Input ~ 0
VCC
Text GLabel 6050 4050 3    50   Input ~ 0
GND
Text GLabel 6550 3350 2    50   Input ~ 0
MISO
Text GLabel 6550 3450 2    50   Input ~ 0
MOSI
Text GLabel 6550 3550 2    50   Input ~ 0
SCK
Text GLabel 2550 1750 2    50   Input ~ 0
MOSI
Text GLabel 2550 1850 2    50   Input ~ 0
MISO
Text GLabel 2550 1950 2    50   Input ~ 0
SCK
$Comp
L Mechanical:MountingHole H1
U 1 1 5EEF35A8
P 1200 5650
F 0 "H1" H 1300 5696 50  0000 L CNN
F 1 "MountingHole" H 1300 5605 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1200 5650 50  0001 C CNN
F 3 "~" H 1200 5650 50  0001 C CNN
	1    1200 5650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5EEF40AC
P 1200 5850
F 0 "H2" H 1300 5896 50  0000 L CNN
F 1 "MountingHole" H 1300 5805 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1200 5850 50  0001 C CNN
F 3 "~" H 1200 5850 50  0001 C CNN
	1    1200 5850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5EEF422D
P 1200 6050
F 0 "H3" H 1300 6096 50  0000 L CNN
F 1 "MountingHole" H 1300 6005 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1200 6050 50  0001 C CNN
F 3 "~" H 1200 6050 50  0001 C CNN
	1    1200 6050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5EEF4486
P 1200 6250
F 0 "H4" H 1300 6296 50  0000 L CNN
F 1 "MountingHole" H 1300 6205 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1200 6250 50  0001 C CNN
F 3 "~" H 1200 6250 50  0001 C CNN
	1    1200 6250
	1    0    0    -1  
$EndComp
NoConn ~ 2550 1450
NoConn ~ 2550 2050
NoConn ~ 2550 2150
NoConn ~ 2550 2350
NoConn ~ 2550 2450
NoConn ~ 2550 2550
NoConn ~ 2550 2650
NoConn ~ 2550 3850
NoConn ~ 2550 3750
NoConn ~ 2550 3650
NoConn ~ 2550 3550
NoConn ~ 2550 3450
NoConn ~ 2550 3350
NoConn ~ 1350 1450
Text GLabel 650  950  1    50   Input ~ 0
VCC
Text GLabel 650  1250 3    50   Input ~ 0
GND
$Comp
L Device:C C1
U 1 1 5EEEC0FF
P 650 1100
F 0 "C1" H 765 1146 50  0000 L CNN
F 1 "10uF" H 765 1055 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 688 950 50  0001 C CNN
F 3 "~" H 650 1100 50  0001 C CNN
	1    650  1100
	1    0    0    -1  
$EndComp
NoConn ~ 2550 1550
$Comp
L Regulator_Linear:TC1054 U4
U 1 1 5EEF9CE8
P 3150 4800
F 0 "U4" H 3150 5125 50  0000 C CNN
F 1 "TC1054" H 3150 5034 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 3150 5075 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21350E.pdf" H 2850 5000 50  0001 C CNN
	1    3150 4800
	1    0    0    -1  
$EndComp
Text GLabel 3150 5100 3    50   Input ~ 0
GND
Text GLabel 2750 4700 0    50   Input ~ 0
VIn
Text GLabel 4550 950  1    50   Input ~ 0
VCC
Text GLabel 3550 4700 2    50   Input ~ 0
VCC
Text GLabel 2550 1650 2    50   Input ~ 0
~SHDN
Text GLabel 2750 4800 0    50   Input ~ 0
~SHDN
$Comp
L Connector:USB_B_Micro J3
U 1 1 5EF05D9F
P 1350 4750
F 0 "J3" H 1407 5217 50  0000 C CNN
F 1 "USB_B_Micro" H 1407 5126 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex_47346-0001" H 1500 4700 50  0001 C CNN
F 3 "~" H 1500 4700 50  0001 C CNN
	1    1350 4750
	1    0    0    -1  
$EndComp
Text GLabel 1350 5150 3    50   Input ~ 0
GND
Text GLabel 1250 5150 3    50   Input ~ 0
GND
Text GLabel 1650 4550 2    50   Input ~ 0
VIn
NoConn ~ 1650 4750
NoConn ~ 1650 4950
NoConn ~ 1650 4850
NoConn ~ 3550 4800
$EndSCHEMATC
