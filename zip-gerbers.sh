#!/usr/bin/env bash

PROJ="avr-atmospheric-sensor"
zip -9m ${PROJ}-gerber.zip *gbr* *drl
